package org.avinash.assignment;
import org.testng.Assert;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import java.util.*;

public class TestCustomReentrantReadWriteLock {
    ICustomReentrantReadWriteLock lock = new CustomReentrantReadWriteLock();
    @Test(testName = "TEST1: RELEASE READ LOCK WITHOUT ACQUIRING IT ",
          expectedExceptions = {IllegalMonitorStateException.class},
            expectedExceptionsMessageRegExp="Requesting Thread : TESTCASE1THREAD is not owning read lock and hence can't release it")
    public void a_testReleaseReadLockWithoutAcquiring() {
        String expectedExceptionMsg = "Requesting Thread : TESTCASE1THREAD is not owning read lock and hence can't release it";
        final boolean[] exceptionOccured = {false};
        Thread test = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    lock.releaseReadLock();
                } catch(InterruptedException iex){
                    System.out.println("InterruptedException");
                }
                catch(Exception ex){
                    Assert.assertEquals(expectedExceptionMsg,ex.getMessage());
                    exceptionOccured[0] = true;
                }
            }
        });
        test.setName("TESTCASE1THREAD");
        test.start();
        try {
            test.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(exceptionOccured[0])
            throw  new IllegalMonitorStateException(expectedExceptionMsg);
    }

    @Test(testName = "TEST2: RELEASE WRITE LOCK WITHOUT ACQUIRING IT ",
            expectedExceptions = {IllegalMonitorStateException.class},
            expectedExceptionsMessageRegExp="NO_THREAD_OWNS_WRITE_LOCK")
    public void b_testReleaseWriteLockWithoutAcquiring(){
        String expectedExceptionMsg = "NO_THREAD_OWNS_WRITE_LOCK";
        final boolean[] exceptionOccured = {false};
        Thread test = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    lock.releaseWriteLock();
                } catch (InterruptedException e) {
                    System.out.println(" InterruptedException e : "+e.getMessage());
                }
                catch (Exception ex) {
                    Assert.assertEquals(expectedExceptionMsg,ex.getMessage());
                    exceptionOccured[0]=true;
                }
            }
        });
        test.setName("TESTCASE2THREAD");
        test.start();
        try {
            test.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(exceptionOccured[0])
            throw new IllegalMonitorStateException(expectedExceptionMsg);
    }
    @Test(testName = "TEST3: RELEASE WRITE LOCK ACQUIRED BY ANOTHER THREAD ",
            expectedExceptions = {IllegalMonitorStateException.class},
            expectedExceptionsMessageRegExp="Requesting Thread : TESTCASE3THREAD1 is different from owner of  the write lock on this ReadWriteLock . Owner Thread : TESTCASE3THREAD")
    public void c_testReleaseAcquiredWriteLockWithoutAcquiring(){
        String expectedExceptionMsg = "Requesting Thread : TESTCASE3THREAD1 is different from owner of  the write lock on this ReadWriteLock . Owner Thread : TESTCASE3THREAD";
        final boolean[] exceptionOccured = {false};
        Thread test = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    lock.acquireWriteLock();
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    System.out.println(" InterruptedException e : "+e.getMessage());
                }
                catch (Exception ex) {
                    Assert.assertEquals(expectedExceptionMsg,ex.getMessage());
                    exceptionOccured[0]=true;
                }
                finally {
                    try {
                        lock.releaseWriteLock();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        test.setName("TESTCASE3THREAD");
        test.start();

        Thread test1 = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    lock.releaseWriteLock();
                } catch (InterruptedException e) {
                    System.out.println(" InterruptedException e : "+e.getMessage());
                }
                catch (Exception ex) {
                    Assert.assertEquals(expectedExceptionMsg,ex.getMessage());
                    exceptionOccured[0]=true;
                }
            }
        });
        test1.setName("TESTCASE3THREAD1");
        test1.start();
        try {
            test1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(exceptionOccured[0])
            throw new IllegalMonitorStateException(expectedExceptionMsg);
    }
    @Test(testName = "TEST4: ACQUIRE_MULTIPLE_READ_LOCKS ")
    public void d_testAcquireMultipleReadLocks(){
        Thread test = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    lock.acquireReadLock();
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    System.out.println(" InterruptedException e : "+e.getMessage());
                }
                catch (Exception ex) {
                    System.out.println(" Exception ex : "+ex.getMessage());
                }
                finally {
                    try {
                        lock.releaseReadLock();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        test.setName("TESTCASE4THREAD");
        test.start();

        Thread test1 = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    lock.acquireReadLock();
                } catch (InterruptedException e) {
                    System.out.println(" InterruptedException e : "+e.getMessage());
                }
                catch (Exception ex) {
                    System.out.println(" Exception ex : "+ex.getMessage());
                }
                finally {
                    try {
                        lock.releaseReadLock();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        test1.setName("TESTCASE4THREAD1");
        test1.start();
    }

}
