package org.avinash.assignment;
import java.util.HashMap;
import java.util.Map;

/**
 * 1. Should Allow multiple thread concurrent Read only IF NO Write is in progress
 * 2. Should allow writes by only one thread only if NO READ is in progress
 * */
public class CustomReentrantReadWriteLock implements ICustomReentrantReadWriteLock {
    /* Map of Reader Threads and corresponding Request counts */
    private Map<Thread, Integer> readingThreadRequestCountMap = new HashMap<Thread, Integer>();
    /* single writer thread */
    private Thread writerThread = null;
    /* should be either 0 or 1 depending on writerThread is null or not-null */
    private int writeInProgressCount    = 0;

    /* Method to acquire ReadLock by Thread */
    @Override
    public synchronized void acquireReadLock() throws InterruptedException{
        Thread requestingThread = Thread.currentThread();
        /* unless ReadAccess is granted,checkForReadAccess(requestingThread) is true , requestingThread should wait */
        while(!checkForReadLockAccess(requestingThread)){
            wait();
        }
        readingThreadRequestCountMap.put(requestingThread,(readingThreadRequestCountMap.getOrDefault(requestingThread,0) + 1));
        System.out.println("READ_LOCK_ACQUIRED_BY_THREAD : "+requestingThread.getName());
    }

    public synchronized void releaseReadLock() throws InterruptedException {
        Thread requestingThread = Thread.currentThread();
        if(readingThreadRequestCountMap.get(requestingThread) == null){
            throw new IllegalMonitorStateException("Requesting Thread : "+requestingThread.getName()+" is not owning read lock and hence can't release it");
        }
        int adjustedRequestCountForRequestThread = readingThreadRequestCountMap.get(requestingThread)-1;
        if(adjustedRequestCountForRequestThread == 0){
            readingThreadRequestCountMap.remove(requestingThread);
        }
        else {
            readingThreadRequestCountMap.put(requestingThread, (adjustedRequestCountForRequestThread));
        }
        notifyAll();
        System.out.println("READ_LOCK_RELEASED_BY_THREAD : "+requestingThread.getName());
    }

    public synchronized void acquireWriteLock() throws InterruptedException{
        Thread requestingThread = Thread.currentThread();
        /* unless ReadAccess is granted,checkForReadAccess(requestingThread) is true , requestingThread should wait */
        while(!checkForWriteLockAccess(requestingThread)){
            wait();
        }
        /*once writeAccess is allowed , assign requestingThread as writingThread */
        writeInProgressCount++;
        writerThread = requestingThread;
        System.out.println("WRITE_LOCK_ACQUIRED_BY_THREAD : "+requestingThread.getName());

    }
    public synchronized void releaseWriteLock() throws InterruptedException{
        /* Thread should be allowed to release write lock only if this is thread which has acquired it, else throw exception */
        if(writerThread==null){
            throw new IllegalMonitorStateException("NO_THREAD_OWNS_WRITE_LOCK");
        }
        Thread requestingThread = Thread.currentThread();
        if(requestingThread != writerThread && writerThread != null ){
            throw new IllegalMonitorStateException("Requesting Thread : " +requestingThread.getName()+" is different from owner of  the write lock on this ReadWriteLock . Owner Thread : "+writerThread.getName());
        }
        writeInProgressCount--;
        if(writeInProgressCount == 0){
            writerThread = null;
        }
        notifyAll();
        System.out.println("WRITE_LOCK_RELEASED_BY_THREAD : "+Thread.currentThread().getName());
    }

    private boolean checkForReadLockAccess(Thread requestingThread){
        /* READ_LOCK_REQUEST is done by same thread which is writingThread : return true */
        if( requestingThread == writerThread ) return true;
        /* READ_LOCK_REQUEST by THREAD while SOME_OTHER_THREAD is WRITING : return false */
        if( writerThread != null) return false;
        /* READ_LOCK_REQUEST by THREAD PRESENT IN readingThreadRequestCountMap while NO_WRITING_THREAD : return true */
        if( readingThreadRequestCountMap.get(requestingThread) != null ) return true;
        //if( hasWriteRequests()) return false;
        return true;
    }

    private boolean checkForWriteLockAccess(Thread requestingThread){
        /* WRITE_LOCK_REQUEST is done by  thread which is only readerThread : return true */
        if(readingThreadRequestCountMap.get(readingThreadRequestCountMap)!=null && readingThreadRequestCountMap.size()==1)    return true;
        /* WRITE_LOCK_REQUEST is done by  thread while multiple readerThreads other than writerThread  : return false */
        if(readingThreadRequestCountMap.size()>0) return false;
        /* WRITE_LOCK_REQUEST is done by  thread with no reader thread and writer thread Assigned : return true */
        if(writerThread == null)   return true;
        /* WRITE_LOCK_REQUEST is done by  thread but a writing thread currently assigned : return false */
        if(requestingThread != writerThread)       return false;
        return true;
    }
}

