package org.avinash.assignment;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ClientCustomReentrantReadWriteLock {
    class CustomCache {
        private final Map<String, String> cache = new HashMap<>();
        ICustomReentrantReadWriteLock lock = new CustomReentrantReadWriteLock();
        public String get(String key) throws InterruptedException {
            try {
                lock.acquireReadLock();
                return cache.get(key);
            } finally {
                lock.releaseReadLock();
            }
        }
        public Set<String> allKeys() throws InterruptedException {
            try {
                lock.acquireReadLock();
                return cache.keySet();
            } finally {
                lock.releaseReadLock();
            }
        }
        public String put(String key, String value) throws InterruptedException {
            try {
                lock.acquireWriteLock();
                return cache.put(key, value);
            } finally {
                lock.releaseWriteLock();
            }
        }
        public void clear() throws InterruptedException {
            try {
                lock.acquireWriteLock();
                cache.clear();
            } finally {
                lock.releaseWriteLock();
            }
        }
    }
    public static void main(String[] args) {
        ClientCustomReentrantReadWriteLock client = new ClientCustomReentrantReadWriteLock();
        ClientCustomReentrantReadWriteLock.CustomCache cache = client.new CustomCache();
        Thread producerThread = new Thread(new Runnable(){
            @Override
            public void run(){
                try {
                    while(true){
                        String currentTime = String.valueOf(System.currentTimeMillis());
                        cache.put(currentTime,"Procuded At : "+System.currentTimeMillis());
                        System.out.println(" Producer added currentTime : "+currentTime);
                        Thread.sleep(1000);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread keyPrinterThread = new Thread(new Runnable(){
            @Override
            public void run(){
                try {
                    while(true){
                        System.out.println(" Available keys at this time : "+cache.allKeys());
                        Thread.sleep(5000);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread cleanerThread = new Thread(new Runnable(){
            @Override
            public void run(){
                try {
                    while(true)
                    {System.out.println(" cleaning cache intermittently time current entries size : "+cache.allKeys().size());
                        cache.clear();
                        Thread.sleep(10000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        producerThread.setName("PRODUCER");
        keyPrinterThread.setName("KEYPRINTER");
        cleanerThread.setName("CLEANERTHREAD");
        producerThread.start();
        keyPrinterThread.start();
        cleanerThread.start();
    }
}

