package org.avinash.assignment;
public interface ICustomReentrantReadWriteLock {
    public  void  acquireReadLock()throws InterruptedException;

    public  void  releaseReadLock()throws InterruptedException;

    public  void  acquireWriteLock()throws InterruptedException;

    public  void  releaseWriteLock()throws InterruptedException;

}



