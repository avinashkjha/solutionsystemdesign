Sample Library For Re-entrant Read Write Lock Project
==========================================================

TestCases : mvn test
This will execute 4 test cases which are mentioned in Test Class TestCustomReentrantReadWriteLock
TEST1: RELEASE READ LOCK WITHOUT ACQUIRING IT
TEST2: RELEASE WRITE LOCK WITHOUT ACQUIRING IT
TEST3: RELEASE WRITE LOCK ACQUIRED BY ANOTHER THREAD
TEST4: ACQUIRE_MULTIPLE_READ_LOCKS
----------------------------------------------------------------------------------------------------------------------------
Other than this I have tried to showcase usage of this lock in
Class ClientCustomReentrantReadWriteLock
where Three different threads are operating on a cache which is being guarded by this lock
producerThread      :  keeps on adding some key,value pair in cache at some interval
keyPrinterThread    :  prints all the keys available in cache periodically
cleanerThread       :  removes all entry from cache periodically

